+++
author = "Dylan Binh"
title = "Video: hướng dẫn sử dụng Bootmapper Client"
date = "2021-09-23"
description = "Dùng bootmapper để sửa lỗi không nhận diện bàn phím"
draft = false
language = "vi"
categories = [
    "build",
]
+++

Bootmapper Client là phần mềm để tương tác với các phím như KBD75 R1, GSX96 R2, J80S, Dolphin R1,... . Bootmapper Client giúp các bạn có thể upload firmware, map lại nút, điều chỉnh led hoặc fix lỗi khi windows không nhận diện phím...

Tác giả của video này là Hoàng Ngự Bình, một trong những assembler có tiếng bậc nhất trong cộng đồng phím cơ Việt Nam. Hiện Bình đang sống và làm việc tại Hà Nội. Bình có phong cách trình bày lưu loát, chuẩn xác và mạch lạc. Các bạn cùng xem và tìm hiểu thêm nhé:

{{< youtube gLIRP4v6Sf8 >}}

---  

<br>*Credit:* 
ASMR - Mechanical Keyboard - Dylan Binh, https://www.youtube.com/channel/UCG6fx-9OblDMkp3s3hyY2zw

Link download Bootmapper Client: https://winkeyless.kr/firmware-installing-and-setting/  
Link hướng dẫn tắt Windows Update: https://tinhte.vn/thread/windows-10-huong-dan-cach-tat-windows-update.2484829/
